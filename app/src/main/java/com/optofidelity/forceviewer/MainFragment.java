package com.optofidelity.forceviewer;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class MainFragment extends Fragment {
    TextView mForceValBottom;
    TextView mXValBottom;
    TextView mYValBottom;
    TextView mForceValTop;
    TextView mXValTop;
    TextView mYValTop;
    ProgressBar mProgBar;
    RelativeLayout mainView;
    ImageView mImgView;

    GridLayout mGrid1;
    GridLayout mGrid2;
    GridLayout mGrid3;
    GridLayout mGrid4;

    private DecimalFormat df = new DecimalFormat("####.##");

    float forceScaleFactor;

    public MainFragment() {
        forceScaleFactor = (float) 1.0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mGrid3 = (GridLayout) rootView.findViewById(R.id.gridLayout3);
        mGrid4 = (GridLayout) rootView.findViewById(R.id.gridLayout4);
        mForceValBottom = (TextView) rootView.findViewById(R.id.textView);
        mXValBottom = (TextView) rootView.findViewById(R.id.textViewXVal);
        mYValBottom = (TextView) rootView.findViewById(R.id.textViewYVal);
        mForceValTop = (TextView) rootView.findViewById(R.id.textViewForceValTop);
        mXValTop = (TextView) rootView.findViewById(R.id.textViewXTopVal);
        mYValTop = (TextView) rootView.findViewById(R.id.textViewYTopVal);
        mProgBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        mProgBar.setMax(1000);
        return rootView;
    }

    public void setForceScaleFactor(float fctr){
        forceScaleFactor = fctr;
    }

    public float getForceScaleFactor(){
        return forceScaleFactor;
    }

    public void setX(int val){
        mXValBottom.setText(Integer.toString(val));
        mXValTop.setText(Integer.toString(val));
    }

    public void setY(int val){
        mYValBottom.setText(Integer.toString(val));
        mYValTop.setText(Integer.toString(val));
    }

    public void setForceBackground(float val){
        if(mainView == null) {
            try {
                mainView = (RelativeLayout) getView().findViewById(R.id.mainfragment);
                setBackground(val);
            } catch (NullPointerException e) {
                mainView = null;
            }
        }
        else{
            setBackground(val);
        }
    }

    public void setForceText(float val){
        mForceValTop.setText(df.format(val * forceScaleFactor));
        mForceValBottom.setText(df.format(val * forceScaleFactor));
    }

    public void setForceBar(float val){
        int scaledVal = (int) (1000 * val);
        mProgBar.setProgress(scaledVal);
    }

    private void setBackground(float forceVal){
        int col = Color.argb(Math.round(255 * forceVal), 255, 0, 0);
        mainView.setBackgroundColor(col);
    }
}
package com.optofidelity.forceviewer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import java.util.LinkedList;
import java.util.Queue;


public class MainActivity extends Activity {
    private MainFragment mFragment;
    private Float prevForceVal = (float) 0.0;
    private Queue<Float> mForceQueue = new LinkedList<Float>();
    private float forceUpdateIntervalMillis = 100;
    private long prevTouchTime = 0;

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFragment = new MainFragment();
        getFragmentManager().beginTransaction()
                .add(R.id.container, mFragment)
                .commit();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onResume(){
        super.onResume();
        Intent intent = getIntent();
        String message = intent.getStringExtra(SettingsActivity.SCALE_FACTOR);
        if (message.length() > 0){
            mFragment.setForceScaleFactor(Float.parseFloat(message));
        }
        else {
            mFragment.setForceScaleFactor((float) 1.0);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_UP:
                mFragment.setX(0);
                mFragment.setY(0);
                mFragment.setForceBackground((float) 0.0);
                //mFragment.setForceBar((float) 0.0);
                mFragment.setForceText((float) 0.0);
                mForceQueue.clear();
            case MotionEvent.ACTION_DOWN:
                break;
            default:
                float prs = e.getPressure();
                int x = Math.round(e.getX());
                int y = Math.round(e.getY());

                mForceQueue.add(prs);
                int qSize = mForceQueue.size();
                Long timeMs = System.currentTimeMillis();

                if ((timeMs - prevTouchTime >= forceUpdateIntervalMillis)) {
                    Float avgForce = (float) 0.0;

                    for (Float hVal : mForceQueue) {
                        avgForce += hVal;
                    }
                    mForceQueue.clear();
                    avgForce /= (float) qSize;
                    prevForceVal = avgForce;
                    prevTouchTime = timeMs;
                }

                mFragment.setX(x);
                mFragment.setY(y);
                mFragment.setForceBackground(prs);
                //mFragment.setForceBar(prs);
                mFragment.setForceText(prevForceVal);
        }

        return true;
    }
}
